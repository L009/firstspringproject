package springproject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main  {

/*        public static void main(String[] args) {
            MessageRenderer mr = new StandartOutMessageRenderer();
            MessageProvider mp = new HellowWorldMessageProvider();

            mr.setMessageProvider(mp);
            mr.render();
        }*/


//    public static void main(String[] args) {
//        MessageRenderer mr = MessageSupportFactory.getInstance().
//                getMessageRenderer();
//        MessageProvider mp = MessageSupportFactory.getInstance().
//                getMessageProvider();
//
//        mr.setMessageProvider(mp);
//        mr.render();
//    }

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext
                ("app-context.xml");

        MessageRenderer mr = ctx.getBean("renderer", MessageRenderer.class);
        mr.render();
    }
}
