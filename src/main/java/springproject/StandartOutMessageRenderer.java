package springproject;

public class StandartOutMessageRenderer implements MessageRenderer {
    private MessageProvider messageProvider;

    public void render() {
        if(messageProvider == null) {
            throw new RuntimeException("Your must set the property messageProvider of class:"
                + StandartOutMessageRenderer.class.getName()
            //Вы должны установить свойство messageProvider класса
            );
        }
        System.out.println(this.messageProvider.getMessage());
    }

    public void setMessageProvider(MessageProvider provider) {
        this.messageProvider = provider;
    }

    public MessageProvider getMessageProvider() {
        return this.messageProvider;
    }
}
