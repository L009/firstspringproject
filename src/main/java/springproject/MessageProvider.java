package springproject;

public interface MessageProvider {
    String getMessage();
}
